/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualremote;

import java.lang.reflect.Array;
import java.util.Scanner;
import virtualremote.command.Command;
import virtualremote.command.NullCommand;
import virtualremote.userprograms.*;

/**
 *
 * @author nielsrobben
 */
public class VirtualRemote {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        
        Command[] keymap = new Command[9];
        
        for (int x = 0; x < keymap.length; x++){
            keymap[x] = new NullCommand();
        }
        
        keymap[0] = new SprinklerRemote();
        keymap[1] = new MainThermostat();
        keymap[2] = new SprinklerRemote();
        
        
//        SprinklerRemote remote1 = new SprinklerRemote();
//        remote1.execute();
        
        while(true){
         
        System.out.println("Please press a key which is > 0 and < 9 ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        System.out.println("");
        
        switch(input){
            case 0: keymap[0].execute();
                break;
            case 1: keymap[1].execute(); 
                break;
            case 2: keymap[2].execute();
                break;
            case 3: keymap[3].execute(); 
                break;
            case 4: keymap[4].execute();
                break;
            case 5: keymap[5].execute(); 
                break;
            case 6: keymap[6].execute(); 
                break;
            case 7: keymap[7].execute();
                break;
            case 8: keymap[8].execute();
                break;
            case 9:  keymap[9].execute();
                break;
            default: break;
        }
        
        System.out.println("\n---------");
        }
        
    }
    
}
