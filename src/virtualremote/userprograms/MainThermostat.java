/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualremote.userprograms;

import virtualremote.command.Command;
import virtualremote.intelliware.Thermostat;

/**
 *
 * @author nielsrobben
 */
public class MainThermostat extends Thermostat implements Command{

    @Override
    public void execute() {
        turnUp();
    }
    
}
