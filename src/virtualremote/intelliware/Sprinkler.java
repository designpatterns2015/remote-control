/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualremote.intelliware;

public class Sprinkler{

    public void on(){
        System.out.println("Turning sprinkler on..");
    }
    public void off(){
        System.out.println("Turning sprinkler off..");
    }
    public void setTimer(int seconds){
        System.out.println("Setting timer for " + seconds + " seconds!");
    }
}
