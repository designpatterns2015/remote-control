/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualremote.intelliware;

/**
 *
 * @author nielsrobben
 */
public class Thermostat {
    public void turnUp(){
        System.out.println("Turning up the thermostat");
    }
    
    public void turnDown(){
        System.out.println("Turning down the thermostat");
    }
}
